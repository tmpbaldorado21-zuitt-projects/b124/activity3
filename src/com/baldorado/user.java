package com.baldorado;

public class user {
        private String fName;
        private String lName;
        private long contactNo;

        public user(String newFName, String newLName, long newContactNo) {
            this.fName = newFName;
            this.lName = newLName;
            this.contactNo = newContactNo;
        }

        public String getFName() {
            return this.fName;
        }

        public String getLName() {
            return this.lName;
        }

        public long getContactNO() {
            return this.contactNo;
        }

        public String userName() {
            return this.fName + " " + this.lName;
        }

        public long userContactNo() {
            return this.contactNo;
        }
}
