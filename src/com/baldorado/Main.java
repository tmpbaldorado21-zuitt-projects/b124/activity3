package com.baldorado;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        user person1 = new user("John", "Doe", 9759024002L);
        user person2 = new user("Jane", "Doe", 9348782727L);

        System.out.println("** Person 1 Details **");
        System.out.print("Name: " + person1.userName() + "\n");
        System.out.print("Contact Number: " + person1.userContactNo() + "\n");

        System.out.println("\n** Person 2 Details **");
        System.out.print("Name: " + person2.userName() + "\n");
        System.out.print("Contact Number: " + person2.userContactNo() + "\n");

        ArrayList<String> usersList = new ArrayList<String>();
        usersList.add(person1.userName());
        usersList.add(person2.userName());

        System.out.println("\n** Users List **");
        for(String name: usersList) {
            System.out.println(name);
        }
    }
}
